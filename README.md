# Lettergen

Generate scrlltr2 letters like a pro!

Lettergen takes an input file in yaml format and generates a beatifully typeset
pdf using the excellent LaTeX komascript class scrlttr2.

## Dependencies

### Runtime

- Texlive (core and latexextra)
- libyaml
- pcre
- gc (boehm garbage collector)

### Build

- Crystal (and Shards)

## Install

## ArchLinux

Install `lettergen` from AUR.

## Other systems

Clone the project and run `make && make install`

## Usage

```
Usage: lettergen [options] letter.yml
    -s FILE, --stamp-file=FILE       Use stamp from FILE
    -p PAGE, --stamp-page=PAGE       Use stamp from PAGE (default: 1)
    -r ROW, --stamp-row=ROW          Use stamp from ROW (default: 1)
    -c COLUMN, --stamp-column=COLUMN Use stamp from COLUMN (default: 1)
    -h, --help                       Show this message
```

### Example

```
$> genletter path/to/my-letter.yml
```

This will generate a pdf `path/to/my-letter.pdf`

For an example output see [examples/letter.pdf](examples/letter.pdf)

## YAML Format

See [examples/letter.yml](examples/letter.yml) for a generated example using all
currently supported options.

The following options are optional:

- from.phone
- from.email
- from.url
- from.bank
- from.customer
- date (this will default to the current date)

You can write prettier yaml than the one in
[examples/letter.yml](examples/letter.yml) by making use of the `|-` yaml
modifier:

``` yaml
text: |-
  This is a multi-line text.
  Newlines are preserved.
```

Any newlines in any `address` or `bank` field will be replaced with ` \\\n`. So
LaTeX will force a line break.

## Advanced Topics

### Postage

You can use "Internetmarke" from Deutsche Post to include a digital stamp in
your letter. See `lettergen --help` for details. __NOTE:__ The layout of the
pdf was changed by the Deutsche Post so the current package postage.sty won't
work. There is however [a pull request addressing this
issue](https://github.com/heinrichreimer/latex-postage/pull/4). One possible
workaround is to download the file `postage.sty` from the fork and install it to
`TEXMFHOME`. See https://texfaq.org/FAQ-privinst.

### Modifying the template

If you're not satisfied with the LaTeX template, you can clone this repository
and modify [src/letter.tex.ecr](src/letter.tex.ecr) to your liking. This template will be baked into
the binary, so you'll have to recompile.

## License

Licensed under [GPLv3](COPYING)

Copyright (c) 2020  Joakim Repomaa
