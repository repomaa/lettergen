FROM crystallang/crystal:0.33.0-alpine AS build

COPY shard.yml shard.lock ./
RUN shards install --production
COPY src ./src
RUN shards build --release --static --production

FROM texlive/texlive-full
COPY --from=build bin/lettergen /bin

ENTRYPOINT ["/bin/lettergen"]
