# Lettergen - Generate scr2lttr letters like a pro!
#
# Copyright (c) 2020  Joakim Repomaa
#
# This file is part of Lettergen.
#
# Lettergen is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lettergen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lettergen.  If not, see <https://www.gnu.org/licenses/>.

require "./letter"
require "file_utils"
require "option_parser"

stamp_page = 1
stamp_row = 1
stamp_column = 1
stamp_file = nil

parser = OptionParser.new do |p|
  p.banner = "Usage: #{PROGRAM_NAME} [options] letter.yml"

  p.on("-s FILE", "--stamp-file=FILE", "Use stamp from FILE") do |value|
    stamp_file = value
  end

  p.on("-p PAGE", "--stamp-page=PAGE", "Use stamp from PAGE (default: 1)") do |value|
    stamp_page = value.to_i
  end

  p.on("-r ROW", "--stamp-row=ROW", "Use stamp from ROW (default: 1)") do |value|
    stamp_row = value.to_i
  end

  p.on("-c COLUMN", "--stamp-column=COLUMN", "Use stamp from COLUMN (default: 1)") do |value|
    stamp_column = value.to_i
  end

  p.on("-h", "--help", "Show this message") do
    puts p.to_s
    exit
  end
end

parser.parse
path = ARGV[0]?
abort(parser.to_s) if path.nil?

tempdir = File.join(Dir.tempdir, File.tempname("lettergen"))

File.open(path) do |file|
  letter = Letter.from_yaml(file)
  stamp_file.try do |sf|
    letter.stamp = Letter::Stamp.new(sf, stamp_page, stamp_row, stamp_column)
  end

  Dir.mkdir_p(tempdir)
  jobname = File.basename(path, ".yml")

  status = Process.run(
    "lualatex", [
      "--output-directory", tempdir,
      "--halt-on-error",
      "--jobname", jobname,
    ],
    output: Process::Redirect::Inherit,
    error: Process::Redirect::Inherit,
  ) do |lualatex|
    letter.to_s(STDERR)
    letter.to_s(lualatex.input)
  end

  raise "Failed to create pdf" unless $?.success?
  FileUtils.cp(
    File.join(tempdir, "#{jobname}.pdf"),
    File.join(File.dirname(path), "#{jobname}.pdf")
  )
ensure
  Dir[File.join(tempdir, "**", "*")].each { |file| File.delete(file) }
  Dir.rmdir(tempdir)
end
