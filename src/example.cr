# Lettergen - Generate scr2lttr letters like a pro!
#
# Copyright (c) 2020  Joakim Repomaa
#
# This file is part of Lettergen.
#
# Lettergen is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lettergen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lettergen.  If not, see <https://www.gnu.org/licenses/>.

require "faker"
require "./letter"

class Letter
  class Recepient
    def initialize(*, @name, @address)
    end
  end

  class Sender
    def initialize(*, @name, @address, @phone, @email, @url, @bank, @customer)
    end
  end

  def initialize(
    *, @paper, @format, from, to, @language, @subject, @opening, @text,
    @closing, @date = Time.local, @place
  )
    @from = Sender.new(**from)
    @to = Recepient.new(**to)
  end
end

def generate_address
  String.build do |io|
    io << Faker::Address.street_address << '\n'
    io << Faker::Address.postcode << ' ' << Faker::Address.city
  end
end

def generate_bank
  String.build do |io|
    country_code = Faker::Address.country_code
    io << "IBAN: "
    io << country_code << Faker::Number.number(2) << ' '
    4.times { io << Faker::Number.number(4) << ' ' }
    io << Faker::Number.number(2) << '\n'

    io << "BIC: "
    io << Faker::Lorem.characters(4).upcase
    io << country_code << Faker::Number.number(2) << "XXX" << '\n'

    io << "Bank: " << Faker::Company.name
  end
end

letter = Letter.new(
  paper: "a4",
  format: "DIN",
  from: {
    name: Faker::Name.name,
    address: generate_address,
    phone: Faker::PhoneNumber.phone_number,
    email: Faker::Internet.email,
    url: Faker::Internet.url(path: "/"),
    bank: generate_bank,
    customer: Faker::Number.number(8),
  },
  to: {
    name: Faker::Name.name,
    address: generate_address,
  },
  language: "english",
  subject: Faker::Lorem.sentence(3, false, 2)[0..-2],
  opening: "To whom it might concern,",
  text: Faker::Lorem.paragraphs(3).join("\n\n"),
  closing: "Yours sincerely",
  date: Time.local,
  place: Faker::Address.city,
)

File.open("examples/letter.yml", "w+") do |yaml|
  letter.to_yaml(yaml)
end

File.open("examples/letter.tex", "w+") do |tex|
  letter.to_s(tex)
end
