# Copyright (c) 2020  Joakim Repomaa
#
# This file is part of Lettergen.
#
# Lettergen is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Lettergen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Lettergen.  If not, see <https://www.gnu.org/licenses/>.

require "ecr"
require "yaml"

class Letter
  include YAML::Serializable

  class Sender
    include YAML::Serializable

    getter name : String
    getter address : String
    getter phone : String?
    getter email : String?
    getter url : String?
    getter bank : String?
    getter customer : String?
  end

  class Recepient
    include YAML::Serializable

    getter name : String
    getter address : String
  end

  record Stamp, file : String, page : Int32 = 1, row : Int32 = 1, column : Int32 = 1

  @[YAML::Field(ignore: true)]
  property stamp : Stamp?
  getter paper : String
  getter format : String
  getter from : Sender
  getter to : Recepient
  getter language : String
  getter subject : String
  getter opening : String
  getter text : String
  getter closing : String
  getter date : Time = Time.local
  getter place : String

  ECR.def_to_s "#{__DIR__}/letter.tex.ecr"
end
