PREFIX := /usr

bin/lettergen: src/letter.cr src/letter.tex.ecr src/main.cr
	shards build --release --production

install: bin/lettergen
	install -Dm755 bin/lettergen $(PREFIX)/bin/lettergen

examples/example.pdf: examples/letter.tex
	lualatex -output-directory examples -halt-on-error $^

examples/letter.yml:
	crystal src/example.cr

examples/letter.tex:
	crystal src/example.cr

.PHONY: install examples/letter.yml examples/letter.tex
